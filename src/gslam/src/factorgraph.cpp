#include "gslam/factorgraph.h"

namespace factorgraph{

/*template<typename T>
decltype(auto) normalize_pose(T&& pose)
{
	if (pose > 2 * M_PI) return std::forward<T>(pose) - 2* M_PI;
	else return std::forward<T>(pose) + 2 * M_PI;
}*/ 

void normalize_pose(Eigen::VectorXd& pose)
{
	if (pose(2) > 2 * M_PI) pose(2) -= 2 * M_PI;
	if (pose(2) <= 0) pose(2) += 2 * M_PI;
}

Eigen::VectorXd add_relative_pose(Eigen::VectorXd& absolute, Eigen::VectorXd& relative)
{
	// Pass by the reference and only return bool
	double new_theta = absolute(2) + relative(2);
	if (new_theta > M_PI) new_theta -= 2 * M_PI;
	if (new_theta <= -M_PI) new_theta += 2 * M_PI;
	Eigen::VectorXd toReturn(3);
	double delta_x = cos(absolute(2)) * relative(0) - sin(absolute(2)) * relative(1);
    double delta_y = sin(absolute(2)) * relative(0) + cos(absolute(2)) * relative(1);
	toReturn << absolute(0)+delta_x,
	            absolute(1)+delta_y,
	            new_theta;
	return toReturn;
}


FactorGraph::FactorGraph(bool debug) : _debug(debug), _initial_poses{}, 
						_relative_edges{}, _graph_backend{}, _graph_estimates{}, _parameters{}, _isam{_parameters}
{
	Eigen::VectorXd initial(3);
	initial.fill(0.0);
	_initial_poses.push_back(initial);
	_parameters.setRelinearizeThreshold(0.0001);
    _parameters.setRelinearizeSkip(1);

    _graph_estimates.insert(1, gtsam::Pose2(_initial_poses[0](0), _initial_poses[0](1), _initial_poses[0](2)));
        // Declaring the Mean:
    gtsam::Pose2 priorMean(0.0, 0.0, 0.0);
        // Declaring the Noise:
    Eigen::VectorXd sigmas(3);
    sigmas.fill(0.0);
    //gtsam::noiseModel::Diagonal priorNoise(sigmas);
    auto priorNoise = gtsam::noiseModel::Diagonal::Sigmas(sigmas);
    _graph_backend.add(PriorFactorPose2(1, priorMean, priorNoise));
}

FactorGraph::~FactorGraph(){}

std::vector<Eigen::VectorXd> FactorGraph::get_current_estimate()
{
	return this->_initial_poses;
}

std::vector<Eigen::VectorXd> FactorGraph::optimize(int steps=1)
{
	this->mtx.lock();
	std::vector<Eigen::VectorXd> result{};
	Eigen::VectorXd pose_vector(3);
	try
	{
		this->_isam.update(this->_graph_backend, this->_graph_estimates);
		for(int i = 0; i < steps; i++)
		{
			this->_isam.update();
		}

		auto current_estimate = this->_isam.calculateEstimate(); // gstam::Values
		for(const auto k : current_estimate.keys())
		{
			auto pose = current_estimate.at<gtsam::Pose2>(k);
			pose_vector << pose.x(), pose.y(), pose.theta();
			result.push_back(pose_vector);
		}

		this->_initial_poses = result;
		this->_graph_backend = gtsam::NonlinearFactorGraph();
		this->_graph_estimates.clear();
	} catch (std::exception &e)
	{
		std::cerr << "Unhandled Exception: " << e.what() << std::endl;
	}
	this->mtx.unlock();
	return result;
}

bool FactorGraph::append_relative_pose(Eigen::VectorXd& pose, Eigen::Matrix3d covariance, int from_id=-1)
{
    ROS_INFO("%.2f", sqrt(pose(0) * pose(0) + pose(1) * pose(1)));
	this->mtx.lock();
	if (from_id == -1) from_id = _initial_poses.size() - 1;
	normalize_pose(pose);
	int idx = this->get_current_index()+1;
	Eigen::VectorXd global_pos = add_relative_pose(this->_initial_poses[from_id], pose);

	/*Eigen::VectorXd global_pos = [_initial_poses[from_id], &pose](Eigen::VectorXd absolute, Eigen::VectorXd relative) -> Eigen::VectorXd
	{
		double new_theta = absolute(2) + relative(2);
		if (new_theta > M_PI) new_theta -= 2 * M_PI;
		if (new_theta <= -M_PI) new_theta += 2 * M_PI;
		Eigen::VectorXd toReturn(3);
		double delta_x = cos(absolute(2)) * relative(0) - sin(absolute(2)) * relative(1);
    	double delta_y = sin(absolute(2)) * relative(0) + cos(absolute(2)) * relative(1);
		toReturn << absolute(0)+delta_x,
	            	absolute(1)+delta_y,
	            	new_theta;
		return toReturn;
	}*/

	// Insert pose into _initial_estimates
	this->_initial_poses.push_back(global_pos);
	this->_graph_estimates.insert(idx, gtsam::Pose2(global_pos(0), global_pos(1), global_pos(2)));

	// Insert relative edge to the graph:
	bool state = this->insert_relative_pose(from_id, idx, pose, covariance, true);
	if (!state) std::cout << "Error at factorgraph.cpp append_relative_pose function, state is False" << std::endl;
	this->mtx.unlock();
	return state;


}

bool FactorGraph::insert_relative_pose(int from_id, int to_id, Eigen::VectorXd& pose, Eigen::Matrix3d covariance, bool locked=false)
{
	normalize_pose(pose);
	if (!locked) this->mtx.lock();
	if (from_id < 1 || to_id < 1 || from_id > this->get_current_index() || to_id > this->get_current_index()
		|| from_id == to_id)
	{
		if(!locked) this->mtx.unlock();
		return false;
	}
	Eigen::VectorXd sigmas(3);
	sigmas << covariance(0,0), covariance(1,1), covariance(2,2);
	auto noise = gtsam::noiseModel::Diagonal::Sigmas(sigmas);
	this->_graph_backend.add(gtsam::BetweenFactor<gtsam::Pose2>(from_id, to_id, gtsam::Pose2(pose(0), pose(1), pose(2)), noise));
	this->_relative_edges.push_back(from_to_edges(from_id, to_id, gtsam::Pose2(pose(0), pose(1), pose(2))));

	if (!locked) this->mtx.unlock();
	return true;
}

int FactorGraph::get_current_index()
{
	return this->_initial_poses.size();
}

/*void FactorGraph::add_absolute_position(int to_id, Eigen::VectorXd position)
{
	this->mtx.lock();

}*/

} // end namespace