#include "gslam/factorgraph.h"

/*template<typename T>
class Array
{
private:
	T* data;
public:
	Array(int size)
	{
		if (size > 0) data = new T[size];
	}
	~Array()
	{
		delete[] data;
	}
};*/


template<typename T>
class Array
{
private:
	std::unique_ptr<T> data;
public:
	Array(int size)
	{
		if (size > 0) data(new T(size));
	}
	Array(const Array& other)
	{
		this->data(new T(*other.data));
	}
};

void find_int(std::vector<int>& numbres, int& epsilon)
{
	std::find_if(numbres.begin(), numbres.end(), [&epsilon](int number) -> bool 
		{ 
			if (number%2 == 1) 
			{
			  epsilon = number; 
			  return true;
		    } 
		   else return false;
		});
}
int main(int agrc, char** argv)
{
	//FactorGraph fg(true);
	return 0;
}