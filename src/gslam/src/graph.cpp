#include "gslam/graph.h"

namespace graphslam
{

GraphSLAM::GraphSLAM(ros::NodeHandle nh, bool publish_tf=true) : _nh(nh), _publish_tf(publish_tf),
                        _graph{}, _last_ori{}, _last_pos{}, _last_odom_pose{},
                        _relocalization_required(false), _min_covariance_pose{},
                        _min_covariance_main_pose{}, _min_covariance_between_factor{}, _last_idx{}, _last_pose{}

{
	Eigen::VectorXd toInsert(3);
	toInsert << 0, 0, 0;
	this->_last_pose.emplace("main_pose", toInsert);
	this->_last_pos = toInsert;
	this->_last_odom_pose = toInsert;
	toInsert.resize(4);
	toInsert << 0, 0, 0, 1;
	this->_last_ori = toInsert;

	Eigen::Matrix3d covariance;
	covariance << 0.01 * 0.01, 1e-8, 1e-8,
	              1e-8, 0.01 * 0.01, 1e-8,
	              1e-8, 1e-8, (2/(M_PI*180))*(2/(M_PI*180));
	this->_min_covariance_pose = covariance;
	covariance(2,2) = (10/(M_PI*180))*(10/(M_PI*180));
	this->_min_covariance_main_pose = covariance;
	covariance(2,2) = (5/(M_PI*180))*(5/(M_PI*180));
	this->_min_covariance_between_factor = covariance;

	std::string main_pose = "odom";
	std::string between_topic = "between_factor";
	std::string poses = "scanmatch";

	//if (_nh.getParam("odometry", main_pose));
	//if (_nh.getParam("poses", poses));

	this->_path_pub = _nh.advertise<nav_msgs::Path>("trajectory", 1);
	this->_path_2d_pub = _nh.advertise<gslam_msgs::Path2D>("trajectory2", 1);
	this->_pose_pub = _nh.advertise<geometry_msgs::PoseStamped>("pose", 1);
	this->_pose_node_pub = _nh.advertise<gslam_msgs::PoseNode>("pose_node", 1);

	this->_main_pose_sub = nh.subscribe(main_pose, 1000, &GraphSLAM::main_pose_callback, this);
	this->_between_topic_sub = nh.subscribe(between_topic, 1000, &GraphSLAM::between_callback, this);
	this->_pcl2_sub = nh.subscribe("cloud", 100, &GraphSLAM::pcl_callback, this);

}

void GraphSLAM::publish(std::vector<Eigen::VectorXd> &result, std_msgs::Header header)
{
    nav_msgs::Path curPath{};
    gslam_msgs::Path2D curPath2D;
    curPath.header.frame_id = "map";
    curPath2D.header.frame_id = "map";
    geometry_msgs::PoseStamped lastPose{};
    Eigen::VectorXd lastMapPose{};

    lastPose.header.stamp = header.stamp;
    lastPose.header.frame_id = "map";

    for(auto const &matrix : result)
    {
        geometry_msgs::Pose2D p2;
        p2.x = matrix(0);
        p2.y = matrix(1);
        p2.theta = matrix(2);
        curPath2D.poses.push_back(p2);
        geometry_msgs::PoseStamped p;
        p.header.stamp = header.stamp;
        p.header.frame_id = "map";
        p.pose.position.x = matrix(0);
        p.pose.position.y = matrix(1);
        p.pose.position.z = 0;
        tf2::Quaternion myQuaternion;
        myQuaternion.setRPY(0,0,matrix(2));
        p.pose.orientation.x = myQuaternion.x();
        p.pose.orientation.y = myQuaternion.y();
        p.pose.orientation.z = myQuaternion.z();
        p.pose.orientation.w = myQuaternion.w();
        curPath.poses.push_back(p);
        lastPose = p;
        lastMapPose = matrix;
    }

    this->_path_pub.publish(curPath);
    this->_path_2d_pub.publish(curPath2D);
    this->_pose_pub.publish(lastPose);

    geometry_msgs::PoseStamped p;
    auto delta = lastMapPose(2) - this->_last_odom_pose(2);
    auto c = cos(delta);
    auto s = sin(delta);
    auto px = lastMapPose(0);
    auto py = lastMapPose(1);
    auto lx = c * this->_last_odom_pose(0) - s * this->_last_odom_pose(1);
    auto ly = s * this->_last_odom_pose(0) + c * this->_last_odom_pose(1);
    p.header.stamp = header.stamp;
    p.header.frame_id = "map";
    p.pose.position.x = px - lx;
    p.pose.position.y = py - ly;
    p.pose.position.z = 0;

    tf2::Quaternion myQuaternion;
    myQuaternion.setRPY(0,0,delta);
    p.pose.orientation.x = myQuaternion.x();
    p.pose.orientation.y = myQuaternion.y();
    p.pose.orientation.z = myQuaternion.z();
    p.pose.orientation.w = myQuaternion.w();

    if (this->_publish_tf)
    {
        static tf2_ros::TransformBroadcaster br;
        geometry_msgs::TransformStamped ts;
        ts.header.stamp = ros::Time::now();
        ts.header.frame_id = "map";
        ts.child_frame_id = "odom";
        ts.transform.translation.x = p.pose.position.x;
        ts.transform.translation.y = p.pose.position.y;
        ts.transform.translation.z = p.pose.position.z;
        //tf2::Quaternion q;
        //q.setRPY(this->,0, );
        ts.transform.rotation.x = p.pose.orientation.x;
        ts.transform.rotation.y = p.pose.orientation.y;
        ts.transform.rotation.z = p.pose.orientation.z;
        ts.transform.rotation.w = p.pose.orientation.w;

        br.sendTransform(ts);

        this->_last_ori(0) = p.pose.orientation.x;
        this->_last_ori(1) = p.pose.orientation.y;
        this->_last_ori(2) = p.pose.orientation.z;
        this->_last_ori(3) = p.pose.orientation.w;

        this->_last_pos(0) = p.pose.position.x;
        this->_last_pos(1) = p.pose.position.y;
        this->_last_pos(2) = p.pose.position.z;
    }

    ROS_INFO("publish");

}

void GraphSLAM::pcl_callback(const sensor_msgs::PointCloud2::ConstPtr &data)
{
    this->scan = *data;
}

void GraphSLAM::main_pose_callback(const nav_msgs::Odometry::ConstPtr& data)
{
	if (this->_relocalization_required) return;
	Eigen::Quaternionf acOrient(data->pose.pose.orientation.x, data->pose.pose.orientation.y,
								data->pose.pose.orientation.z, data->pose.pose.orientation.w);
	Eigen::Vector3f acEuler = Eigen::Matrix3f(acOrient).eulerAngles(0,1,2);
	Eigen::VectorXd pose(3);
	pose << data->pose.pose.position.x, data->pose.pose.position.y, acEuler(2);

	ROS_INFO("Main_pose_callback");

	Eigen::VectorXd rel_pose = [](Eigen::VectorXd& pose1, Eigen::VectorXd& pose2) -> Eigen::VectorXd
	{
		double x = pose2(0) - pose1(0);
		double y = pose2(1) - pose1(1);
		double dtheta = pose2(2) - pose1(2);

		Eigen::VectorXd toReturn(3);
		double delta_x = cos(pose2(2)) * x - sin(pose2(2)) * y;
		double delta_y = sin(pose2(2)) * x + cos(pose2(2)) * y;
		toReturn << delta_x, delta_y, dtheta;
		return toReturn;
	}(this->_last_pose["main_pose"],pose);

	//auto rel_pose = rel_pose_func(this->_last_pose["main_pose"],pose);

	if (this->_publish_tf)
	{
		static tf2_ros::TransformBroadcaster br;
		geometry_msgs::TransformStamped ts;
		ts.header.stamp = ros::Time::now();
		ts.header.frame_id = "map";
		ts.child_frame_id = "odom";
		ts.transform.translation.x = this->_last_pos(0);
		ts.transform.translation.y = this->_last_pos(1);
		ts.transform.translation.z = this->_last_pos(2);
		//tf2::Quaternion q;
		//q.setRPY(this->,0, );
		ts.transform.rotation.x = this->_last_ori(0);
		ts.transform.rotation.y = this->_last_ori(1);
		ts.transform.rotation.z = this->_last_ori(2);
		ts.transform.rotation.w = this->_last_ori(3);

		br.sendTransform(ts);
	}

	if((sqrt(pose(0) * pose(0) + pose(1) * pose(1)) < 0.3)) return;
	this->_last_pose["main_pose"] = rel_pose;

	Eigen::Matrix3d covariance;
	covariance << std::max(data->pose.covariance[0], this->_min_covariance_main_pose(0,0)), std::max(data->pose.covariance[1],this->_min_covariance_main_pose(0,1)), std::max(data->pose.covariance[5],this->_min_covariance_main_pose(0,2)),
            std::max(data->pose.covariance[6], this->_min_covariance_main_pose(1,0)), std::max(data->pose.covariance[7],this->_min_covariance_main_pose(1,1)), std::max(data->pose.covariance[11],this->_min_covariance_main_pose(1,2)),
            std::max(data->pose.covariance[30], this->_min_covariance_main_pose(2,0)), std::max(data->pose.covariance[31],this->_min_covariance_main_pose(2,1)), std::max(data->pose.covariance[35],this->_min_covariance_main_pose(2,2));


	gslam_msgs::PoseNode poseNode;
	poseNode.id = _graph.get_current_index();
	poseNode.scan = this->scan;
	this->_pose_node_pub.publish(poseNode);
	// TEMP!!!
	Eigen::VectorXd Temp(3);
	Temp << 0, 0, 0;

	this->_last_odom_pose = pose;
	this->_mtx.lock();
	this->_graph.append_relative_pose(rel_pose, covariance, -1);
	auto result = this->_graph.optimize(1);
	this->_mtx.unlock();
	this->publish(result, data->header);


}

void GraphSLAM::between_callback(const gslam_msgs::BetweenFactor::ConstPtr &data)
{
    Eigen::VectorXd rel_pose(3);
    rel_pose << data->pose.x, data->pose.y, data->pose.theta;
    auto last_idx = data->from_id;
    auto current_idx = data->to_id;
    Eigen::Matrix3d covariance;
    covariance << std::max<float>(data->covariance[0], this->_min_covariance_between_factor(0,0)), std::max<float>(data->covariance[1], this->_min_covariance_between_factor(0,1)), std::max<float>(data->covariance[2], this->_min_covariance_between_factor(0,2)),
            std::max<float>(data->covariance[3], this->_min_covariance_between_factor(1,0)), std::max<float>(data->covariance[4], this->_min_covariance_between_factor(1,1)), std::max<float>(data->covariance[5], this->_min_covariance_between_factor(1,2)),
            std::max<float>(data->covariance[6], this->_min_covariance_between_factor(2,0)), std::max<float>(data->covariance[7], this->_min_covariance_between_factor(2,1)), std::max<float>(data->covariance[8], this->_min_covariance_between_factor(2,2));

    this->_mtx.lock();
    if (this->_relocalization_required && current_idx == this->_graph.get_current_index() + 1)
    {
        this->_graph.append_relative_pose(rel_pose, covariance, last_idx);
        this->_relocalization_required = false;
    } else
    {
        this->_graph.insert_relative_pose(last_idx, current_idx, rel_pose, covariance, false);
    }

    auto result = this->_graph.optimize(10);
    this->_mtx.unlock();
    this->publish(result, data->header);
}

}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "gslam");
	ros::NodeHandle nh;
	graphslam::GraphSLAM slam(nh, true);
	ros::spin();
	return 0;
}