#ifndef GRAPH_H_
#define GRAPH_H_

#include <iostream>
#include <vector>
#include <string>
#include <map>

#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Pose2D.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <gslam_msgs/PoseNode.h>
#include <std_msgs/Header.h>

#include "gslam/factorgraph.h"
#include "gslam_msgs/BetweenFactor.h"
#include "gslam_msgs/Path2D.h"


# define M_PI 3.14159265358979323846

namespace graphslam
{

class GraphSLAM{
private:
	ros::NodeHandle _nh;
	bool _publish_tf;
	factorgraph::FactorGraph _graph;
	Eigen::VectorXd _last_ori;
	Eigen::VectorXd _last_pos;
	Eigen::VectorXd _last_odom_pose;
	bool _relocalization_required;
	Eigen::Matrix3d _min_covariance_pose;
	Eigen::Matrix3d _min_covariance_main_pose;
	Eigen::Matrix3d _min_covariance_between_factor;
	std::map<std::string,int> _last_idx;
	std::map<std::string,Eigen::VectorXd> _last_pose;


    sensor_msgs::PointCloud2 scan;
	ros::Subscriber _main_pose_sub;
	ros::Subscriber _between_topic_sub;
	ros::Subscriber _pcl2_sub;
	ros::Publisher _path_pub;
	ros::Publisher _path_2d_pub;
	ros::Publisher _pose_pub;
	ros::Publisher _pose_node_pub;

	std::mutex _mtx;

public:
	GraphSLAM(ros::NodeHandle nh, bool publish_tf);

	void publish(std::vector<Eigen::VectorXd>&, std_msgs::Header);
	void main_pose_callback(const nav_msgs::Odometry::ConstPtr& data);
	void between_callback(const gslam_msgs::BetweenFactor::ConstPtr& data);
	void pcl_callback(const sensor_msgs::PointCloud2::ConstPtr& data);

};

} // namespace
#endif
