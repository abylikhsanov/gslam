#ifndef FACTORGRAPH_H_
#define FACTORGRAPH_H_

#include <ros/ros.h>
#include <gtsam/geometry/Pose2.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/ISAM2Params.h>
#include <gtsam/nonlinear/ISAM2.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <thread>
#include <cmath>
#include <mutex>
#include <array>
#include <vector>
#include <Eigen/Core>
#include "gslam_msgs/PoseNode.h"


# define M_PI 3.14159265358979323846

typedef gtsam::PriorFactor<gtsam::Pose2> PriorFactorPose2;

namespace factorgraph{

template<typename T>
decltype(auto) normalize_pose(T&& pose);

struct from_to_edges
{
	from_to_edges(int from_id, int to_id, gtsam::Pose2 pose) : from_id(from_id), to_id(to_id), pose(pose){}
	int from_id;
	int to_id;
	gtsam::Pose2 pose;
};

class FactorGraph
{
private:
	bool _debug;
	std::mutex mtx;
	std::vector<Eigen::VectorXd> _initial_poses;
	std::vector<from_to_edges> _relative_edges;
	gtsam::NonlinearFactorGraph _graph_backend;
	gtsam::Values _graph_estimates;
	gtsam::ISAM2Params _parameters;
	gtsam::ISAM2 _isam;

public:
	FactorGraph(bool debug);
	FactorGraph() = default;
	FactorGraph(const FactorGraph& other) = delete;
	FactorGraph& operator=(const FactorGraph& other) = delete;
	~FactorGraph();

	std::vector<Eigen::VectorXd> get_current_estimate();
	std::vector<Eigen::VectorXd> optimize(int);
	bool append_relative_pose(Eigen::VectorXd& pose, Eigen::Matrix3d covariance, int from_id);
	bool insert_relative_pose(int from_id, int to_id, Eigen::VectorXd& pose, Eigen::Matrix3d covariance, bool locked);
	int get_current_index();

};

}

#endif